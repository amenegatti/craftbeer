import { Beer } from '../interfaces/beer';

const baseUrl = process.env.REACT_APP_API_BASE_URL;

export default class Api {
    static getBeersList(): Promise<Beer[]> {
        const uri = `${baseUrl}/beers`;

        return fetch(uri, { method: 'GET' }).then((response) => {
            return response.json();
        });
    }
}
