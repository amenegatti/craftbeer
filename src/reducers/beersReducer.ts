import { State } from '../interfaces/state/state';
import { initialState } from './../store/state';
import {
    BeersAction,
    FETCH_BEERS_REQUEST,
    FETCH_BEERS_SUCCESS,
    FETCH_BEERS_ERROR,
    SET_CURRENT_PAGE,
} from '../interfaces/actions/actions';
import moment from 'moment';
import { SET_CURRENT_FILTER } from './../interfaces/actions/actions';

export default function beersReducer(state: State = initialState, action: BeersAction): State {
    switch (action.type) {
        case FETCH_BEERS_REQUEST:
            return {
                ...state,
                loading: true,
                error: '',
            };

        case FETCH_BEERS_SUCCESS:
            return {
                ...state,
                beers: action.payload,
                loading: false,
                error: '',
            };

        case FETCH_BEERS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.message,
            };

        case SET_CURRENT_PAGE:
            const page = action.page || 1;

            const source =
                state.filter.from.length > 0 && state.filter.to.length > 0
                    ? state.beers.filter(
                          (beer) =>
                              moment(beer.first_brewed, 'MM/YYYY').isSameOrAfter(
                                  moment(state.filter.from, 'MM/YYYY'),
                              ) &&
                              moment(beer.first_brewed, 'MM/YYYY').isSameOrBefore(moment(state.filter.to, 'MM/YYYY')),
                      )
                    : state.beers;

            const pages = Math.ceil(source.length / state.page.pageSize);

            const pageContent = source.slice((page - 1) * state.page.pageSize, page * state.page.pageSize);

            return {
                ...state,
                page: {
                    currentPage: page,
                    pageSize: state.page.pageSize,
                    pages,
                    pageContent,
                },
            };

        case SET_CURRENT_FILTER:
            return {
                ...state,
                filter: action.data,
            };

        default:
            return state;
    }
}
