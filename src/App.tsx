import React, { useEffect } from 'react';
import './App.css';
import { useSelector, useDispatch } from 'react-redux';
import { fetchBeers, setCurrentPage, setCurrentFilter } from './actions/beersActions';
import { State } from './interfaces/state/state';
import { BeersState } from './interfaces/state/beersState';
import Card from './components/styled/card';
import CardList from './components/styled/cardContainer';
import Pager from './components/pager';
import Filter from './components/fromToFilter';
import { FilterData } from './interfaces/filterData';
import NoData from './components/nodata';
import Error from './components/error';
import { Loading } from './components/loading';

function App(): JSX.Element {
    const state: State = useSelector<BeersState, State>((state) => state.beersState);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchBeers());
    }, []);

    const pageChange = (event: React.ChangeEvent<unknown>, value: number): void => {
        dispatch(setCurrentPage(value));
    };

    const filterChange = (event: React.MouseEvent<HTMLButtonElement>, data: FilterData): void => {
        dispatch(setCurrentFilter(data));
        dispatch(setCurrentPage());
    };

    const renderFilters = (): JSX.Element => {
        return state.loading ? (
            <></>
        ) : (
            <Filter fromText="Brewed between" toText="and" okHandler={filterChange}></Filter>
        );
    };

    const renderBeerList = (): JSX.Element => {
        return state.loading ? (
            <></>
        ) : state.page.pageContent.length === 0 ? (
            <NoData />
        ) : (
            <CardList>
                {state.page.pageContent.map((beer) => {
                    return (
                        <Card
                            key={beer.id}
                            backgroundImage={beer.image_url}
                            name={beer.name}
                            tagline={beer.tagline}
                            description={beer.description}
                        ></Card>
                    );
                })}
            </CardList>
        );
    };

    const renderPager = (): JSX.Element => {
        return state.loading ? <></> : <Pager items={state.page.pages} onChange={pageChange}></Pager>;
    };

    return (
        <div className="App">
            {state.loading && <Loading />}

            {state.error.length > 0 && <Error message={state.error} />}

            {renderFilters()}

            {renderBeerList()}

            {renderPager()}
        </div>
    );
}

export default App;
