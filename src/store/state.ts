import { State } from './../interfaces/state/state';

export const initialState: State = {
    beers: [],
    loading: false,
    error: '',
    page: {
        currentPage: 1,
        pageSize: 4,
        pages: 0,
        pageContent: [],
    },
    filter: {
        from: '',
        to: '',
    },
};
