import { BeersAction, FETCH_BEERS_REQUEST, SET_CURRENT_PAGE, SET_CURRENT_FILTER } from '../interfaces/actions/actions';
import { FilterData } from './../interfaces/filterData';

export function fetchBeers(): BeersAction {
    return {
        type: FETCH_BEERS_REQUEST,
    };
}

export function setCurrentPage(page?: number): BeersAction {
    return {
        type: SET_CURRENT_PAGE,
        page,
    };
}

export function setCurrentFilter(data: FilterData): BeersAction {
    return {
        type: SET_CURRENT_FILTER,
        data,
    };
}
