import React from 'react';
import { CenteredContainer, ErrorMessage } from './styled/common';

const Error = ({ message }: { message: string }): JSX.Element => {
    return (
        <CenteredContainer>
            <ErrorMessage>{message}</ErrorMessage>
        </CenteredContainer>
    );
};

export default Error;
