import { CenteredContainer } from './styled/common';
import Pagination from '@material-ui/lab/Pagination';
import React from 'react';

type handlerType = (event: React.ChangeEvent<unknown>, page: number) => void;

const Pager = ({ items, onChange }: { items: number; onChange: handlerType }): JSX.Element => {
    return (
        <CenteredContainer>
            <Pagination count={items} onChange={onChange} variant="outlined"></Pagination>
        </CenteredContainer>
    );
};

export default Pager;
