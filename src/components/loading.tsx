import { LoadingMessage, CenteredContainer } from './styled/common';
import React from 'react';

export const Loading = (): JSX.Element => {
    return (
        <CenteredContainer>
            <LoadingMessage>Loading data, please wait...</LoadingMessage>
        </CenteredContainer>
    );
};
