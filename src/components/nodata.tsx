import React from 'react';
import { CenteredContainer, NoDataMessage } from './styled/common';

const NoData = (): JSX.Element => {
    return (
        <CenteredContainer>
            <NoDataMessage>No data was found for the current filters selection</NoDataMessage>
        </CenteredContainer>
    );
};

export default NoData;
