import React, { useState } from 'react';
import { FilterData } from './../interfaces/filterData';
import { CenteredContainer } from './styled/common';
import { FilterContainer, FilterLabel, FilterSelect, FilterButton } from './styled/filter';

type clickHandlerType = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, data: FilterData) => void;
type changeHandleType = (event: React.ChangeEvent<HTMLSelectElement>) => void;

const BuildSelect = ({
    items,
    changeHandler,
    defaultValue,
}: {
    items: number[];
    changeHandler: changeHandleType;
    defaultValue: number;
}): JSX.Element => {
    return (
        <FilterSelect onChange={changeHandler} defaultValue={defaultValue}>
            {items.map((item, index) => {
                return (
                    <option key={index} value={item}>
                        {item}
                    </option>
                );
            })}
        </FilterSelect>
    );
};

const months: number[] = [];
const years: number[] = [];

const minMonth = 1;
const minYear = 1900;
const maxMonth = 12;
const maxYear = new Date().getFullYear();

for (let i = minMonth; i <= maxMonth; i++) {
    months.push(i);
}

for (let i = minYear; i <= maxYear; i++) {
    years.push(i);
}

const FromToFilter = ({
    fromText,
    toText,
    okHandler,
}: {
    fromText: string;
    toText: string;
    okHandler: clickHandlerType;
}): JSX.Element => {
    const [state, setState] = useState({
        monthFrom: minMonth,
        yearFrom: minYear,
        monthTo: maxMonth,
        yearTo: maxYear,
    });

    const changeHandler = (event: React.ChangeEvent<HTMLSelectElement>, prop: string): void => {
        setState({
            ...state,
            [prop]: event.target.value,
        });
    };

    const onOkClick = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
        okHandler(event, { from: `${state.monthFrom}/${state.yearFrom}`, to: `${state.monthTo}/${state.yearTo}` });
    };

    return (
        <CenteredContainer>
            <FilterContainer>
                <FilterLabel>{fromText}</FilterLabel>
                <BuildSelect
                    changeHandler={(event): void => changeHandler(event, 'monthFrom')}
                    items={months}
                    defaultValue={minMonth}
                />
                <BuildSelect
                    changeHandler={(event): void => changeHandler(event, 'yearFrom')}
                    items={years}
                    defaultValue={minYear}
                />
                <FilterLabel>{toText}</FilterLabel>
                <BuildSelect
                    changeHandler={(event): void => changeHandler(event, 'monthTo')}
                    items={months}
                    defaultValue={maxMonth}
                />
                <BuildSelect
                    changeHandler={(event): void => changeHandler(event, 'yearTo')}
                    items={years}
                    defaultValue={maxYear}
                />
                <FilterButton onClick={onOkClick}>Ok</FilterButton>
            </FilterContainer>
        </CenteredContainer>
    );
};

export default FromToFilter;
