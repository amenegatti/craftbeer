import styled, { css } from 'styled-components';
import React from 'react';

const backgroundStyle = css`
    background-color: rgb(235, 235, 235);
    border-radius: 25px;
    padding: 0.5rem;
`;

const Label = styled.span`
    ${backgroundStyle}
`;

export const FilterContainer = styled.div`
    margin: 1rem;
    background-color: rgb(249, 249, 249);
    border-radius: 10px;
    padding: 1rem;
`;

export const FilterLabel = styled(Label)`
    margin: 0.5rem;
`;

export const FilterSelect = (
    props: JSX.IntrinsicAttributes &
        React.ClassAttributes<HTMLSelectElement> &
        React.SelectHTMLAttributes<HTMLSelectElement>,
): JSX.Element => {
    return (
        <Label>
            <select {...props}></select>
        </Label>
    );
};

export const FilterButton = styled.button`
    ${backgroundStyle}
    border: 0;
    margin: 1rem;
    min-width: 70px;
    box-shadow: none;
`;
