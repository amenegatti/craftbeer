/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div<{ backbroundImage?: string }>`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 400px;
    height: 250px;
    border-radius: 4px;
    padding: 1rem;
    margin: 1rem;
    background-color: #c4b2a9;
    background-size: contain;
    background-repeat: no-repeat;
    background-origin: content-box;
    background-position-x: right;
    background-image: ${(props) => `url(${props.backbroundImage})` || 'none'};

    &:hover {
        opacity: 0.5;
        cursor: pointer;
    }
`;

const TitleLabel = styled.h1`
    font-size: 2rem;
    margin: 1rem;
    margin-bottom: 0;
    color: white;
`;

const SubTitleLabel = styled.h3`
    margin: 1rem;
    color: white;
`;

const Title = ({ title, subtitle }: { title?: string; subtitle?: string }) => {
    return (
        <div>
            <TitleLabel>{title}</TitleLabel>
            <SubTitleLabel>{subtitle}</SubTitleLabel>
        </div>
    );
};

const Description = styled.p`
    color: white;
`;

const Card = ({
    backgroundImage,
    name,
    tagline,
    description,
}: {
    backgroundImage?: string;
    name?: string;
    tagline?: string;
    description?: string;
}) => {
    const wordNumber = 20;

    const wordCount = (text: string) => {
        return text.split(' ').length;
    };

    const trimmedDescription = (text: string) => {
        return wordCount(text) > wordNumber ? `${text.split(' ').splice(0, wordNumber).join(' ')}...` : text;
    };

    return (
        <Wrapper backbroundImage={backgroundImage}>
            <Title title={name} subtitle={tagline} />
            <Description>{trimmedDescription(description || '')}</Description>
        </Wrapper>
    );
};

export default Card;
