import styled from 'styled-components';

const messageContainer = styled.div`
    font-weight: bold;
    padding: 2rem;
    border-radius: 10px;
`;

export const CenteredContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
`;

export const NoDataMessage = styled(messageContainer)`
    background-color: grey;
`;

export const ErrorMessage = styled(messageContainer)`
    background-color: red;
    color: white;
`;

export const LoadingMessage = styled(messageContainer)`
    background-color: rgb(69, 220, 115);
    color: black;
`;
