import styled from 'styled-components';
import { CenteredContainer } from './common';

const CardList = styled(CenteredContainer)`
    margin-left: 15em;
    margin-right: 15em;
`;

export default CardList;
