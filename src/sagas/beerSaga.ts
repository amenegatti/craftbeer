import { call, put, takeEvery } from 'redux-saga/effects';
import {
    FETCH_BEERS_SUCCESS,
    FETCH_BEERS_ERROR,
    FETCH_BEERS_REQUEST,
    SET_CURRENT_PAGE,
} from '../interfaces/actions/actions';
import Api from './../api/api';

function* fetchBeers(): Generator {
    try {
        const beers = yield call(Api.getBeersList);
        yield put({ type: FETCH_BEERS_SUCCESS, payload: beers });
        yield put({ type: SET_CURRENT_PAGE, page: 1 });
    } catch (e) {
        console.log('saga: error calling api');
        yield put({ type: FETCH_BEERS_ERROR, message: e.message });
    }
}

function* fetchBeersWatcher(): Generator {
    yield takeEvery(FETCH_BEERS_REQUEST, fetchBeers);
}

export default fetchBeersWatcher;
