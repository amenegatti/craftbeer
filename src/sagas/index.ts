import fetchBeersWatcher from './beerSaga';
import { all } from 'redux-saga/effects';

export default function* rootSaga(): Generator {
    yield all([fetchBeersWatcher()]);
}
