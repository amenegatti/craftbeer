/* eslint-disable @typescript-eslint/camelcase */
import beersReducer from '../../reducers/beersReducer';
import { initialState } from '../../store/state';
import {
    FETCH_BEERS_REQUEST,
    FETCH_BEERS_SUCCESS,
    FETCH_BEERS_ERROR,
    SET_CURRENT_PAGE,
    SET_CURRENT_FILTER,
} from '../../interfaces/actions/actions';
import { Beer } from '../../interfaces/beer';
import { State } from '../../interfaces/state/state';
import { FilterData } from './../../interfaces/filterData';

let beers: Beer[];

beforeAll(() => {
    beers = [
        {
            id: 1,
            image_url: '',
            name: 'beer1',
            tagline: 'some tagline',
            first_brewed: '01/2010',
            description: 'some description',
            ibu: 10,
        },
        {
            id: 2,
            image_url: '',
            name: 'beer2',
            tagline: 'some tagline',
            first_brewed: '01/2018',
            description: 'some description',
            ibu: 10,
        },
        {
            id: 3,
            image_url: '',
            name: 'beer3',
            tagline: 'some tagline',
            first_brewed: '01/2020',
            description: 'some description',
            ibu: 10,
        },
        {
            id: 4,
            image_url: '',
            name: 'beer4',
            tagline: 'some tagline',
            first_brewed: '01/2020',
            description: 'some description',
            ibu: 10,
        },
    ];
});

test('fetch_beer_request should return set loading true', () => {
    const state = beersReducer(initialState, { type: FETCH_BEERS_REQUEST });

    expect(state.loading).toBe(true);
});

test('fetch_beers_success should retun list of beers', () => {
    const state = beersReducer(initialState, { type: FETCH_BEERS_SUCCESS, payload: beers });

    expect(state.beers.length).toBe(4);
});

test('fetch_beers_error shoul return error message', () => {
    const state = beersReducer(initialState, { type: FETCH_BEERS_ERROR, message: 'some error' });

    expect(state.error).toBe('some error');
});

test('set_current_pages should return page 1 if not page provided', () => {
    const state = beersReducer(initialState, { type: SET_CURRENT_PAGE });

    expect(state.page.currentPage).toBe(1);
});

test('set_current_page should calculate number of pages based on the whole list if not filter is provided', () => {
    const newState: State = {
        beers,
        loading: false,
        error: '',
        page: {
            currentPage: 1,
            pageSize: 2,
            pages: 0,
            pageContent: [],
        },
        filter: {
            from: '',
            to: '',
        },
    };

    const state = beersReducer(newState, { type: SET_CURRENT_PAGE });

    expect(state.page.pages).toBe(2);
});

test('set_current_page should calculate number of pages based on the filtered list if filter is provided', () => {
    const newState: State = {
        beers,
        loading: false,
        error: '',
        page: {
            currentPage: 1,
            pageSize: 2,
            pages: 0,
            pageContent: [],
        },
        filter: {
            from: '01/2010',
            to: '01/2018',
        },
    };

    const state = beersReducer(newState, { type: SET_CURRENT_PAGE });

    expect(state.page.pages).toBe(1);
});

test('set_current_page should return the correct page if page number is provided', () => {
    const newState: State = {
        beers,
        loading: false,
        error: '',
        page: {
            currentPage: 1,
            pageSize: 2,
            pages: 0,
            pageContent: [],
        },
        filter: {
            from: '',
            to: '',
        },
    };

    const state = beersReducer(newState, { type: SET_CURRENT_PAGE, page: 2 });

    expect(state.page.pageContent[0].id).toBe(3);
});

test('set_current_page should return correct number of rows', () => {
    const newState: State = {
        beers,
        loading: false,
        error: '',
        page: {
            currentPage: 1,
            pageSize: 2,
            pages: 0,
            pageContent: [],
        },
        filter: {
            from: '',
            to: '',
        },
    };

    const state = beersReducer(newState, { type: SET_CURRENT_PAGE, page: 1 });

    expect(state.page.pageContent.length).toBe(2);
});

test('set_current_filter shoud return filters', () => {
    const data: FilterData = {
        from: '01/2010',
        to: '01/2018',
    };

    const state = beersReducer(initialState, { type: SET_CURRENT_FILTER, data });

    expect(state.filter).toBe(data);
});
