export interface Beer {
    id: number;
    image_url: string;
    name: string;
    tagline: string;
    first_brewed: string;
    description: string;
    ibu: number;
}
