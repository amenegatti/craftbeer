import { FilterData } from './../filterData';
import { Beer } from '../beer';

export const FETCH_BEERS_REQUEST = 'FETCH_BEERS_REQUEST';
export const FETCH_BEERS_SUCCESS = 'FETCH_BEERS_SUCCESS';
export const FETCH_BEERS_ERROR = 'FETCH_BEERS_ERROR';
export const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';
export const SET_CURRENT_FILTER = 'SET_CURRENT_FILTER';

interface FetchBeersRequest {
    type: typeof FETCH_BEERS_REQUEST;
}

interface FetchBeersSuccess {
    type: typeof FETCH_BEERS_SUCCESS;
    payload: Beer[];
}

interface FetchBeersError {
    type: typeof FETCH_BEERS_ERROR;
    message: string;
}

interface SetCurrentPage {
    type: typeof SET_CURRENT_PAGE;
    page?: number;
}

interface SetCurrentFilter {
    type: typeof SET_CURRENT_FILTER;
    data: FilterData;
}

export type BeersAction = FetchBeersRequest | FetchBeersSuccess | FetchBeersError | SetCurrentPage | SetCurrentFilter;
