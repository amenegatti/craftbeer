import { Beer } from '../beer';

export interface State {
    beers: Beer[];
    loading: boolean;
    error: string;
    page: {
        currentPage: number;
        pageSize: number;
        pages: number;
        pageContent: Beer[];
    };
    filter: {
        from: string;
        to: string;
    };
}
